class Calculate:
    @staticmethod
    def circle(r):
        p = 2 * 3.14 * r
        area = 3.14 * r * r
        dic = dict()
        dic["area"] = area
        dic["p"] = p
        return dic

    @staticmethod
    def rectangle(l, b):
        p = 2 * (l + b)
        area = l * b
        print('perimeter =', p, " area= ", area)
        dic = dict()
        dic["area"] = area
        dic["perimeter"] = p
        return dic

    @staticmethod
    def square(s):
        p = 4 * s
        area = s * s
        dic = dict()
        dic["area"] = area
        dic["perimeter"] = p
        return dic

    @staticmethod
    def triangle(s1, s2, s3):
        p = s1 + s2 + s3
        area = 0.5 * s2 * s3
        dic = dict()
        dic["area"] = area
        dic["perimeter"] = p
        return dic